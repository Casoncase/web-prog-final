/*
SQLyog Community v11.22 (64 bit)
MySQL - 10.1.16-MariaDB : Database - final_project
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`final_project` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `final_project`;

/*Table structure for table `cartdetails` */

DROP TABLE IF EXISTS `cartdetails`;

CREATE TABLE `cartdetails` (
  `cartdetail_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `quantity` int(10) NOT NULL,
  `uom_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`cartdetail_id`),
  KEY `cart of cart detail` (`cart_id`),
  KEY `item in cart detail` (`item_id`),
  KEY `uom of cart detail` (`uom_id`),
  CONSTRAINT `cart of cart detail` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`cart_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `item in cart detail` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `uom of cart detail` FOREIGN KEY (`uom_id`) REFERENCES `uomconversions` (`uomconversion_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cartdetails` */

/*Table structure for table `carts` */

DROP TABLE IF EXISTS `carts`;

CREATE TABLE `carts` (
  `cart_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `owner of cart` (`user_id`),
  CONSTRAINT `owner of cart` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `carts` */

insert  into `carts`(`cart_id`,`user_id`) values (6,1),(7,2),(8,3),(11,13),(9,14);

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(50) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `categories` */

insert  into `categories`(`category_id`,`category`) values (1,'HP'),(2,'Laptop'),(3,'Kamera'),(4,'Buku'),(5,'Kabel');

/*Table structure for table `complaints` */

DROP TABLE IF EXISTS `complaints`;

CREATE TABLE `complaints` (
  `complaint_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `issuer_id` int(10) unsigned NOT NULL,
  `transactionDetail_id` int(10) unsigned NOT NULL,
  `content` varchar(300) NOT NULL,
  PRIMARY KEY (`complaint_id`),
  KEY `issuer of complaint` (`issuer_id`),
  KEY `transactionDetail of complaint` (`transactionDetail_id`),
  CONSTRAINT `issuer of complaint` FOREIGN KEY (`issuer_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `transactionDetail of complaint` FOREIGN KEY (`transactionDetail_id`) REFERENCES `transactiondetail` (`transdetail_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `complaints` */

insert  into `complaints`(`complaint_id`,`date`,`issuer_id`,`transactionDetail_id`,`content`) values (1,'2017-06-09',13,6,'test');

/*Table structure for table `items` */

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(50) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `img_url` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `category of item` (`category_id`),
  KEY `owner of item` (`user_id`),
  CONSTRAINT `category of item` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `owner of item` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `items` */

insert  into `items`(`item_id`,`item_name`,`user_id`,`category_id`,`img_url`) values (1,'HP Lenovo ',1,1,'http://hargahpfull.com/wp-content/uploads/2015/12/Harga-Lenovo-A6010.jpg'),(2,'Laptop Samsung',2,2,'https://img.global.news.samsung.com/global/wp-content/uploads/2016/01/Notebook9_Main_1.jpg'),(4,'Kamera Canon',1,3,'https://www.google.co.id/imgres?imgurl=http%3A%2F%2Fimage.priceprice.k-img.com%2Fglobal%2Fimages%2Fp'),(5,'Kabel Tembaga 2mm',1,5,'non'),(6,'HP Samsung',2,1,'https://www.google.co.id/imgres?imgurl=http%3A%2F%2Fstat.homeshop18.com%2Fhomeshop18%2Fimages%2Fprod');

/*Table structure for table `tokens` */

DROP TABLE IF EXISTS `tokens`;

CREATE TABLE `tokens` (
  `token_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(200) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`token_id`),
  KEY `owner of this token` (`user_id`),
  CONSTRAINT `owner of this token` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `tokens` */

insert  into `tokens`(`token_id`,`token`,`user_id`,`created_at`) values (13,'f3bb477e7643dcb86e4119471bebe747e08be6dc32177b365e8a6a3abbb617b',13,'2017-06-09 12:29:14');

/*Table structure for table `transactiondetail` */

DROP TABLE IF EXISTS `transactiondetail`;

CREATE TABLE `transactiondetail` (
  `transdetail_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trans_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `qty` int(10) unsigned NOT NULL,
  `price` float unsigned NOT NULL,
  `uom_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`transdetail_id`),
  KEY `transaction of detail` (`trans_id`),
  KEY `items of transaction` (`item_id`),
  KEY `uom of items in transaction` (`uom_id`),
  CONSTRAINT `items of transaction` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `transaction of detail` FOREIGN KEY (`trans_id`) REFERENCES `transactions` (`trans_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `uom of items in transaction` FOREIGN KEY (`uom_id`) REFERENCES `uomlist` (`uom_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `transactiondetail` */

insert  into `transactiondetail`(`transdetail_id`,`trans_id`,`item_id`,`qty`,`price`,`uom_id`) values (6,3,1,1,50000000,2),(7,5,1,1,5000000,1);

/*Table structure for table `transactions` */

DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `trans_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `status` int(2) unsigned NOT NULL DEFAULT '0',
  `date` date NOT NULL,
  PRIMARY KEY (`trans_id`),
  KEY `buyer` (`user_id`),
  CONSTRAINT `buyer` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `transactions` */

insert  into `transactions`(`trans_id`,`user_id`,`status`,`date`) values (3,13,0,'2017-03-01'),(5,13,0,'2017-03-01');

/*Table structure for table `uomconversions` */

DROP TABLE IF EXISTS `uomconversions`;

CREATE TABLE `uomconversions` (
  `uomconversion_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `uom_id` int(10) unsigned NOT NULL,
  `price` float NOT NULL,
  `stock` int(10) unsigned NOT NULL,
  PRIMARY KEY (`uomconversion_id`),
  KEY `uom detail of uom` (`uom_id`),
  KEY `item of uom` (`item_id`),
  CONSTRAINT `item of uom` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `uom detail of uom` FOREIGN KEY (`uom_id`) REFERENCES `uomlist` (`uom_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `uomconversions` */

insert  into `uomconversions`(`uomconversion_id`,`item_id`,`uom_id`,`price`,`stock`) values (1,1,1,5000000,20),(2,1,2,50000000,3),(3,5,3,20000,5),(4,5,4,18000000,3),(5,5,5,2000,18),(6,6,2,7000000,14),(7,2,6,15000000,17),(9,4,8,11599900,27);

/*Table structure for table `uomlist` */

DROP TABLE IF EXISTS `uomlist`;

CREATE TABLE `uomlist` (
  `uom_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `uom_name` varchar(30) NOT NULL,
  PRIMARY KEY (`uom_id`),
  KEY `category of uom` (`category_id`),
  CONSTRAINT `category of uom` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `uomlist` */

insert  into `uomlist`(`uom_id`,`category_id`,`uom_name`) values (1,1,'Lusin'),(2,1,'Buah'),(3,5,'M'),(4,5,'KM'),(5,5,'Cm'),(6,2,'Buah'),(7,2,'Lusin'),(8,3,'Buah'),(9,3,'Lusin'),(10,4,'Buah'),(11,4,'Lusin');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level` int(2) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`user_id`,`level`,`name`,`password`,`email`,`address`) values (1,0,'budi','5E884898DA28047151D0E56F8DC6292773603D0D6AABBDD62A11EF721D1542D8','budi@gmail.com','jalan budi'),(2,0,'ana','5E884898DA28047151D0E56F8DC6292773603D0D6AABBDD62A11EF721D1542D8','ana@gmail.com','jalan sesuatu'),(3,1,'admin','5E884898DA28047151D0E56F8DC6292773603D0D6AABBDD62A11EF721D1542D8','admin@admin.com','admin'),(13,0,'joko','5E884898DA28047151D0E56F8DC6292773603D0D6AABBDD62A11EF721D1542D8','joko@email.com','jalan mangga'),(14,0,'dodo','421c76d77563afa1914846b010bd164f395bd34c212e5e99ecb9cf173c1d87','dodo@asdf.com','jalan nangka');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
