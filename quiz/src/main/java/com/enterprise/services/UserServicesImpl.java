package com.enterprise.services;

import com.enterprise.dao.UserDao;
import com.enterprise.models.UserModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("UserServices")
@Transactional
public class UserServicesImpl implements UserServices{
    @Autowired
    private UserDao userDao;
    
    @Override
    public void saveUser(UserModel user){
        userDao.saveUser(user);
    }
    
    @Override
    public List<UserModel> findAllUser(){  
        return userDao.findAllUser();
    }
    
    @Override
    public void deleteUserById(int id){
        userDao.deleteUserById(id);
    }
    
    @Override
    public UserModel findById(int id){  
        return userDao.findById(id);
    }
    
    @Override
    public UserModel findByEmail(String email){  
        return userDao.findByEmail(email);
    }
    
    @Override
    public void updateUser(UserModel user){
        userDao.updateUser(user);
    }
}
