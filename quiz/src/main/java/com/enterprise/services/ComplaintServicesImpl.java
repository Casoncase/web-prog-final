/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.dao.ComplaintDao;
import com.enterprise.models.ComplaintModel;
import com.enterprise.models.TransactionDetailModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Cason Kang
 */
@Service("ComplaintServices")
@Transactional
public class ComplaintServicesImpl implements ComplaintServices{
    @Autowired
    private ComplaintDao complaintDao;

    @Override
    public ComplaintModel findComplaintByTransDetail(TransactionDetailModel transDet) {
        return complaintDao.findComplaintByTransDetail(transDet);
    }
    
    @Override
    public void saveComplaint(ComplaintModel complaint){
        complaintDao.saveComplaint(complaint);
    }
    
    @Override
    public List<ComplaintModel> findAllComplaint(){  
        return complaintDao.findAllComplaint();
    }
    
    @Override
    public void deleteComplaintById(int id){
        complaintDao.deleteComplaintById(id);
    }
    
    @Override
    public ComplaintModel findComplaintById(int id){  
        return complaintDao.findComplaintById(id);
    }
    
    @Override
    public void updateComplaint(ComplaintModel complaint){
        complaintDao.updateComplaint(complaint);
    }

}
