/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.dao.TransactionDao;
import com.enterprise.models.TransactionModel;
import com.enterprise.models.UserModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Cason Kang
 */
@Service("TransactionServices")
@Transactional
public class TransactionServicesImpl implements TransactionServices{
    @Autowired
    private TransactionDao transactionDao;
    
    @Override
    public void saveTransaction(TransactionModel transaction){
        transactionDao.saveTransaction(transaction);
    }
    
    @Override
    public List<TransactionModel> findAllTransaction(){  
        return transactionDao.findAllTransaction();
    }
    
    @Override
    public void deleteTransactionById(int id){
        transactionDao.deleteTransactionById(id);
    }
    
    @Override
    public TransactionModel findById(int id){  
        return transactionDao.findById(id);
    }
    
    @Override
    public void updateTransaction(TransactionModel transaction){
        transactionDao.updateTransaction(transaction);
    }

    @Override
    public List<TransactionModel> findAllTransactionOfUser(UserModel user) {
        return transactionDao.findAllTransactionOfUser(user);
    }
}
