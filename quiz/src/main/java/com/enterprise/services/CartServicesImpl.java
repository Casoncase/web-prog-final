/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.dao.UserDao;
import com.enterprise.models.CartModel;
import com.enterprise.models.UserModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.enterprise.dao.CartDao;

/**
 *
 * @author Cason Kang
 */
@Service("CartServices")
@Transactional
public class CartServicesImpl implements CartServices{
    @Autowired
    private CartDao cartDao;
    
    @Override
    public void saveCart(CartModel cart){
        cartDao.saveCart(cart);
    }
    
    @Override
    public List<CartModel> findAllCart(){  
        return cartDao.findAllCart();
    }
    
    @Override
    public void deleteCartById(int id){
        cartDao.deleteCartById(id);
    }
    
    @Override
    public CartModel findById(int id){  
        return cartDao.findById(id);
    }
    
    @Override
    public void updateCart(CartModel cart){
        cartDao.updateCart(cart);
    }

    @Override
    public CartModel findByUser(UserModel user) {
        return cartDao.findByUser(user);
    }
    
}
