/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.dao.CartDetailDao;
import com.enterprise.models.CartDetailModel;
import com.enterprise.models.CartModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Cason Kang
 */

@Service("CartDetailServices")
@Transactional
public class CartDetailServicesImpl implements CartDetailServices{
    @Autowired
    private CartDetailDao cartDetailDao;
    
    @Override
    public void saveCartDetail(CartDetailModel cartDetail){
        cartDetailDao.saveCartDetail(cartDetail);
    }
    
    @Override
    public List<CartDetailModel> findAllCartDetail(){  
        return cartDetailDao.findAllCartDetail();
    }

    @Override
    public List<CartDetailModel> findAllCartDetailOfUser(CartModel cart) {
        return cartDetailDao.findAllCartDetailOfUser(cart);
    }
    
    @Override
    public void deleteCartDetailById(int id){
        cartDetailDao.deleteCartDetailById(id);
    }
    
    @Override
    public CartDetailModel findById(int id){  
        return cartDetailDao.findById(id);
    }
    
    @Override
    public void updateCartDetail(CartDetailModel cartDetail){
        cartDetailDao.updateCartDetail(cartDetail);
    }
}
