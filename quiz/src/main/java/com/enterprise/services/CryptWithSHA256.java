package com.enterprise.services;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CryptWithSHA256 {
   private static MessageDigest md;

   public static String cryptWithSHA256(String pass){
    try {
        md = MessageDigest.getInstance("SHA-256");
        byte[] passBytes = pass.getBytes();
        md.reset();
        byte[] digested = md.digest(passBytes);
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<digested.length;i++){
            sb.append(Integer.toHexString(0xff & digested[i]));
        }
        return sb.toString();
    } catch (NoSuchAlgorithmException ex) {
        Logger.getLogger(CryptWithSHA256.class.getName()).log(Level.SEVERE, null, ex);
    }
        return null;


   }
}