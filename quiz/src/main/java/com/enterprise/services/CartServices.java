/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.models.CartModel;
import com.enterprise.models.UserModel;
import java.util.List;

/**
 *
 * @author Cason Kang
 */
public interface CartServices {
    void saveCart(CartModel cart);
    List<CartModel> findAllCart();
    void deleteCartById(int id);
    CartModel findById(int id);
    void updateCart(CartModel cart);
    CartModel findByUser(UserModel user);
}
