package com.enterprise.services;

import com.enterprise.models.UserModel;
import java.util.List;

public interface UserServices {
    void saveUser(UserModel user);
    List<UserModel> findAllUser();
    void deleteUserById(int id);
    UserModel findById(int id);
    UserModel findByEmail(String email);
    void updateUser(UserModel user);
}
