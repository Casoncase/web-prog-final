package com.enterprise.services;

import com.enterprise.dao.*;
import com.enterprise.models.CartModel;
import com.enterprise.models.TokenModel;
import com.enterprise.models.UserModel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("TokenServices")
public class TokenServicesImpl extends AbstractDao implements TokenServices{
    @Autowired
    private TokenDao dao;
    
    @Override
    public void saveToken(TokenModel token) {
        dao.saveToken(token);
    }

    @Override
    public List<TokenModel> findAllTokens() {
        return dao.findAllTokens();
    }

    @Override
    public void deleteTokensById(int id) {
        dao.deleteTokensById(id);
    }

    @Override
    public TokenModel findById(int id) {
        return dao.findById(id);
    }

    @Override
    public void updateTokens(TokenModel token) {
        dao.updateTokens(token);
    }

    @Override
    public TokenModel findByUser(UserModel user) {
        return dao.findByUser(user);
    }
    
}
