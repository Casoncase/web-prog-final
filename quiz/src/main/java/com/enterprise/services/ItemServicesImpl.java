/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.dao.CategoryDao;
import com.enterprise.dao.ItemDao;
import com.enterprise.models.CategoryModel;
import com.enterprise.models.ItemModel;
import com.enterprise.models.UserModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("ItemServices")
@Transactional
public class ItemServicesImpl implements ItemServices{
    @Autowired
    private ItemDao dao;
    @Autowired
    private CategoryDao catDao;
    
    @Override
    public void saveItem(ItemModel item){
        dao.saveItem(item);
    }
    
    @Override
    public List<ItemModel> findAllItem(){  
        return dao.findAllItem();
    }
    
    @Override
    public void deleteItemById(int id){
        dao.deleteItemById(id);
    }
    
    @Override
    public ItemModel findItemById(int id){  
        return dao.findItemById(id);
    }
    
    @Override
    public List<ItemModel> findItemByName(String name){
        return dao.findItemByName(name);
    }
    
    @Override
    public void updateItem(ItemModel item){
        dao.updateItem(item);
    }
    
    @Override
    public void insertItemAndCategory(String itemName, String categoryName){
        CategoryModel category = new CategoryModel();
        category.setCategory(categoryName);
        catDao.saveCategory(category);
        
        ItemModel item = new ItemModel();
        item.setCategory_id(category);
        item.setItem_name(itemName);
        dao.saveItem(item);
    }

    @Override
    public List<ItemModel> findItemByCategory(String category) {
        CategoryModel categoryMod = new CategoryModel();
        categoryMod = catDao.findCategoryByName(category).get(0);
        return dao.findItemByCategory(categoryMod);
    }

    @Override
    public List<ItemModel> findItemByUser(UserModel user) {
        return dao.findItemByUser(user);
    }
}