/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.dao.UOMListDao;
import com.enterprise.models.CategoryModel;
import com.enterprise.models.UOMListModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Cason Kang
 */
@Service("UOMListServices")
@Transactional
public class UOMListServicesImpl implements UOMListServices{
    @Autowired
    private UOMListDao uomListDao;
    
    @Override
    public void saveUOMList(UOMListModel uomList){
        uomListDao.saveUOMList(uomList);
    }
    
    @Override
    public List<UOMListModel> findAllUOMList(){  
        return uomListDao.findAllUOMList();
    }
    
    @Override
    public void deleteUOMListById(int id){
        uomListDao.deleteUOMListById(id);
    }
    
    @Override
    public UOMListModel findUOMListById(int id){  
        return uomListDao.findUOMListById(id);
    }
    
    @Override
    public void updateUOMList(UOMListModel uomList){
        uomListDao.updateUOMList(uomList);
    }

    @Override
    public List<UOMListModel> findByCategory(CategoryModel categoryModel) {
        return uomListDao.findByCategory(categoryModel);
    }

}
