/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.models.TransactionModel;
import com.enterprise.models.UserModel;
import java.util.List;

/**
 *
 * @author Cason Kang
 */
public interface TransactionServices {
    void saveTransaction(TransactionModel transaction);
    List<TransactionModel> findAllTransaction();
    void deleteTransactionById(int id);
    TransactionModel findById(int id);
    void updateTransaction(TransactionModel transactionDetail);
    public List<TransactionModel> findAllTransactionOfUser(UserModel user);
}
