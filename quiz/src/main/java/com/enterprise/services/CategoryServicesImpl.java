/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.dao.CategoryDao;
import com.enterprise.models.CategoryModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("CategoryServices")
@Transactional
public class CategoryServicesImpl implements CategoryServices{
    @Autowired
    private CategoryDao dao;
    
    @Override
    public void saveCategory(CategoryModel category){
        dao.saveCategory(category);
    }
    
    @Override
    public List<CategoryModel> findAllCategory(){  
        return dao.findAllCategory();
    }
    
    @Override
    public void deleteCategoryById(int id){
        dao.deleteCategoryById(id);
    }
    
    @Override
    public CategoryModel findCategoryById(int id){  
        return dao.findCategoryById(id);
    }
    
    @Override
    public List<CategoryModel> findCategoryByName(String name){
        return dao.findCategoryByName(name);
    }
    
    @Override
    public void updateCategory(CategoryModel category){
        dao.updateCategory(category);
    }

    @Override
    public CategoryModel findSingleCategoryByName(String name) {
        return dao.findCategoryByName(name).get(0);
    }
    
}