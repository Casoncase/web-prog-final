package com.enterprise.services;

import com.enterprise.models.TokenModel;
import com.enterprise.models.UserModel;
import java.util.List;

public interface TokenServices {
    void saveToken(TokenModel token);
    List<TokenModel> findAllTokens();
    void deleteTokensById(int id);
    TokenModel findById(int id);
    void updateTokens(TokenModel token);
    TokenModel findByUser(UserModel user);
}
