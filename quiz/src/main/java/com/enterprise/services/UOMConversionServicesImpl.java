/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.dao.UOMConversionDao;
import com.enterprise.models.ItemModel;
import com.enterprise.models.UOMConversionModel;
import com.enterprise.models.UOMListModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Cason Kang
 */
@Service("UOMConversionServices")
@Transactional
public class UOMConversionServicesImpl implements UOMConversionServices{
    @Autowired
    private UOMConversionDao uomConversionDao;
    
    @Override
    public void saveUOMConversion(UOMConversionModel uomConversion){
        uomConversionDao.saveUOMConversion(uomConversion);
    }
    
    @Override
    public List<UOMConversionModel> findAllUOMConversion(){  
        return uomConversionDao.findAllUOMConversion();
    }
    
    @Override
    public void deleteUOMConversionById(int id){
        uomConversionDao.deleteUOMConversionById(id);
    }
    
    @Override
    public UOMConversionModel findUOMConversionById(int id){  
        return uomConversionDao.findUOMConversionById(id);
    }
    
    @Override
    public void updateUOMConversion(UOMConversionModel uomConversion){
        uomConversionDao.updateUOMConversion(uomConversion);
    }

    @Override
    public UOMConversionModel findByItemAndUom(ItemModel item, UOMListModel uom) {
        return uomConversionDao.findByItemAndUom(item, uom);
    }

    @Override
    public List<UOMConversionModel> findByItem(ItemModel item) {
        return uomConversionDao.findByItem(item);
    }
}
