/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.models.CartDetailModel;
import com.enterprise.models.CartModel;
import java.util.List;

/**
 *
 * @author Cason Kang
 */
public interface CartDetailServices {
    void saveCartDetail(CartDetailModel cartDetail);
    List<CartDetailModel> findAllCartDetail();
    List<CartDetailModel> findAllCartDetailOfUser(CartModel cart);
    void deleteCartDetailById(int id);
    CartDetailModel findById(int id);
    void updateCartDetail(CartDetailModel cartDetail);
}
