/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.models.ItemModel;
import com.enterprise.models.UOMConversionModel;
import com.enterprise.models.UOMListModel;
import java.util.List;

/**
 *
 * @author Cason Kang
 */
public interface UOMConversionServices {
    void saveUOMConversion(UOMConversionModel uomConversion);
    List<UOMConversionModel> findAllUOMConversion();
    void deleteUOMConversionById(int id);
    UOMConversionModel findUOMConversionById(int id);
    UOMConversionModel findByItemAndUom(ItemModel item, UOMListModel uom);
    void updateUOMConversion(UOMConversionModel uomConversion);
    public List<UOMConversionModel> findByItem(ItemModel item);
}
