/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.models.ItemModel;
import com.enterprise.models.UserModel;
import java.util.List;

public interface ItemServices {
    void saveItem(ItemModel item);
    List<ItemModel> findAllItem();
    void deleteItemById(int id);
    ItemModel findItemById(int id);
    List<ItemModel> findItemByName(String name);
    void updateItem(ItemModel item);
    void insertItemAndCategory(String categoryName, String itemName);
    List<ItemModel> findItemByCategory(String category);
    List<ItemModel> findItemByUser(UserModel user);

}