/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.models.ItemModel;
import com.enterprise.models.TransactionDetailModel;
import com.enterprise.models.TransactionModel;
import java.util.List;

/**
 *
 * @author Cason Kang
 */
public interface TransactionDetailServices {
    void saveTransactionDetail(TransactionDetailModel transactionDetail);
    List<TransactionDetailModel> findAllTransactionDetail();
    void deleteTransactionDetailById(int id);
    TransactionDetailModel findTransactionDetailById(int id);
    void updateTransactionDetail(TransactionDetailModel transactionDetail);
    public List<TransactionDetailModel> findAllDetailsOfTransaction(TransactionModel transModel);

    public List<TransactionDetailModel> findTransactionDetailByItem(ItemModel item);
}
