/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.dao.TransactionDetailDao;
import com.enterprise.models.ItemModel;
import com.enterprise.models.TransactionDetailModel;
import com.enterprise.models.TransactionModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Cason Kang
 */
@Service("TransactionDetailServices")
@Transactional
public class TransactionDetailServicesImpl implements TransactionDetailServices{
    @Autowired
    private TransactionDetailDao transactionDetailDao;

    @Override
    public List<TransactionDetailModel> findTransactionDetailByItem(ItemModel item) {
        return transactionDetailDao.findTransactionDetailByItem(item);
    }
    
    @Override
    public void saveTransactionDetail(TransactionDetailModel transactionDetail){
        transactionDetailDao.saveTransactionDetail(transactionDetail);
    }
    
    @Override
    public List<TransactionDetailModel> findAllTransactionDetail(){  
        return transactionDetailDao.findAllTransactionDetail();
    }
    
    @Override
    public void deleteTransactionDetailById(int id){
        transactionDetailDao.deleteTransactionDetailById(id);
    }
    
    @Override
    public TransactionDetailModel findTransactionDetailById(int id){  
        return transactionDetailDao.findTransactionDetailById(id);
    }
    
    @Override
    public void updateTransactionDetail(TransactionDetailModel transactionDetail){
        transactionDetailDao.updateTransactionDetail(transactionDetail);
    }

    @Override
    public List<TransactionDetailModel> findAllDetailsOfTransaction(TransactionModel transModel) {
        return transactionDetailDao.findAllDetailsOfTransaction(transModel);
    }
}
