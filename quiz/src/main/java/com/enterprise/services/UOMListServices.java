/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import com.enterprise.models.CategoryModel;
import com.enterprise.models.UOMListModel;
import java.util.List;

/**
 *
 * @author Cason Kang
 */
public interface UOMListServices {
    void saveUOMList(UOMListModel uomList);
    List<UOMListModel> findAllUOMList();
    List<UOMListModel> findByCategory(CategoryModel categoryModel);
    void deleteUOMListById(int id);
    UOMListModel findUOMListById(int id);
    void updateUOMList(UOMListModel uomList);
    
}
