package com.enterprise.dao;

import com.enterprise.models.CategoryModel;
import com.enterprise.models.UOMListModel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("UOMListDao")
public class UOMListDaoImpl extends AbstractDao implements UOMListDao{
    @Override
    @Transactional
    public void saveUOMList(UOMListModel uomList){
        persist(uomList);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<UOMListModel> findAllUOMList(){
        Criteria criteria = getSession().createCriteria(UOMListModel.class);
        return (List<UOMListModel>) criteria.list();
    }
    
    @Override
    @Transactional
    public void deleteUOMListById(int id){
        Query query = getSession().createSQLQuery("delete from uomlist where uom_id = :id");
        query.setString("id", String.valueOf(id));
        query.executeUpdate();
    }
    
    @Override
    public UOMListModel findUOMListById(int id){
        Criteria criteria = getSession().createCriteria(UOMListModel.class);
        criteria.add(Restrictions.eq("uom_id", id));
        return (UOMListModel) criteria.uniqueResult();
    }
    
    @Override
    @Transactional
    public void updateUOMList (UOMListModel uomList){
        getSession().update(uomList);
    }

    @Override
    public List<UOMListModel> findByCategory(CategoryModel categoryModel) {
        Criteria criteria = getSession().createCriteria(UOMListModel.class);
        criteria.add(Restrictions.eq("category", categoryModel));
        return (List<UOMListModel>) criteria.list();
    }
}
