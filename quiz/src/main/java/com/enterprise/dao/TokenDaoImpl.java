package com.enterprise.dao;

import com.enterprise.models.CartModel;
import com.enterprise.models.TokenModel;
import com.enterprise.models.UserModel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("TokenDao")
public class TokenDaoImpl extends AbstractDao implements TokenDao{

    @Override
    @Transactional
    public void saveToken(TokenModel token) {
        persist(token);
    }

    @Override
    public List<TokenModel> findAllTokens() {
        Criteria criteria = getSession().createCriteria(TokenModel.class);
        return (List<TokenModel>) criteria.list();
    }

    @Override
    @Transactional
    public void deleteTokensById(int id) {
        Query query = getSession().createSQLQuery("delete from tokens where token_id = :id");
        query.setString("id", String.valueOf(id));
        query.executeUpdate();
    }

    @Override
    public TokenModel findById(int id) {
        Criteria criteria = getSession().createCriteria(TokenModel.class);
        criteria.add(Restrictions.eq("token_id", id));
        return (TokenModel) criteria.uniqueResult();
    }

    @Override
    @Transactional
    public void updateTokens(TokenModel token) {
        getSession().update(token);
    }

    @Override
    @Transactional
    public TokenModel findByUser(UserModel user) {
        Criteria criteria = getSession().createCriteria(TokenModel.class);
        criteria.add(Restrictions.eq("user", user));
        return (TokenModel) criteria.uniqueResult();
    }
    
}
