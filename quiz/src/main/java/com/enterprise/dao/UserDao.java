package com.enterprise.dao;

import com.enterprise.models.UserModel;
import java.util.List;

public interface UserDao {
    void saveUser(UserModel user);
    List<UserModel> findAllUser();
    void deleteUserById(int id);
    UserModel findById(int id);
    UserModel findByEmail(String email);
    void updateUser(UserModel user);
}
