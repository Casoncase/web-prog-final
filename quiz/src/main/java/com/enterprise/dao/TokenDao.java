package com.enterprise.dao;

import com.enterprise.models.TokenModel;
import com.enterprise.models.UserModel;
import java.util.List;

public interface TokenDao {
    void saveToken(TokenModel token);
    List<TokenModel> findAllTokens();
    void deleteTokensById(int id);
    TokenModel findById(int id);
    void updateTokens(TokenModel token);
    TokenModel findByUser(UserModel user);
}
