package com.enterprise.dao;

import com.enterprise.models.CartDetailModel;
import com.enterprise.models.CartModel;
import java.util.List;

public interface CartDetailDao {
    void saveCartDetail(CartDetailModel cartDetail);
    List<CartDetailModel> findAllCartDetail();
    List<CartDetailModel> findAllCartDetailOfUser(CartModel cart);
    void deleteCartDetailById(int id);
    CartDetailModel findById(int id);
    void updateCartDetail(CartDetailModel cartDetail);
}
