package com.enterprise.dao;

import com.enterprise.models.ItemModel;
import com.enterprise.models.UserModel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("UserDao")
public class UserDaoImpl extends AbstractDao implements UserDao{
    @Override
    @Transactional
    public void saveUser(UserModel user){
        persist(user);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<UserModel> findAllUser(){
        Criteria criteria = getSession().createCriteria(UserModel.class);
        return (List<UserModel>) criteria.list();
    }
    
    @Override
    @Transactional
    public void deleteUserById(int id){
        Query query = getSession().createSQLQuery("delete from users where user_id = :id");
        query.setString("id", String.valueOf(id));
        query.executeUpdate();
    }
    
    @Override
    public UserModel findById(int id){
        Criteria criteria = getSession().createCriteria(UserModel.class);
        criteria.add(Restrictions.eq("user_id", id));
        return (UserModel) criteria.uniqueResult();
    }
    
    @Override
    public UserModel findByEmail(String email){
       Criteria criteria = getSession().createCriteria(UserModel.class);
        criteria.add(Restrictions.eq("email", email));
        return (UserModel) criteria.uniqueResult();
    }
    
    @Override
    @Transactional
    public void updateUser(UserModel user){
        getSession().update(user);
    }
}
