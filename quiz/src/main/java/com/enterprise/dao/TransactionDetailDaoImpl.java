package com.enterprise.dao;

import com.enterprise.models.ItemModel;
import com.enterprise.models.TransactionDetailModel;
import com.enterprise.models.TransactionModel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("TransactionDetailDao")
public class TransactionDetailDaoImpl extends AbstractDao implements TransactionDetailDao{
    @Override
    @Transactional
    public void saveTransactionDetail(TransactionDetailModel transactionDetail){
        persist(transactionDetail);
    }

    @Override
    public List<TransactionDetailModel> findTransactionDetailByItem(ItemModel item) {
        Criteria criteria = getSession().createCriteria(TransactionDetailModel.class);
        criteria.add(Restrictions.eq("item_id", item));
        return (List<TransactionDetailModel>) criteria.list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<TransactionDetailModel> findAllTransactionDetail(){
        Criteria criteria = getSession().createCriteria(TransactionDetailModel.class);
        return (List<TransactionDetailModel>) criteria.list();
    }
    
    @Override
    @Transactional
    public void deleteTransactionDetailById(int id){
        Query query = getSession().createSQLQuery("delete from transactiondetail where transdetail_id = :id");
        query.setString("id", String.valueOf(id));
        query.executeUpdate();
    }
    
    @Override
    public TransactionDetailModel findTransactionDetailById(int id){
        Criteria criteria = getSession().createCriteria(TransactionDetailModel.class);
        criteria.add(Restrictions.eq("transdetail_id", id));
        return (TransactionDetailModel) criteria.uniqueResult();
    }
    
    @Override
    @Transactional
    public void updateTransactionDetail (TransactionDetailModel transactionDetail){
        getSession().update(transactionDetail);
    }

    @Override
    public List<TransactionDetailModel> findAllDetailsOfTransaction(TransactionModel transModel) {
        Criteria criteria = getSession().createCriteria(TransactionDetailModel.class);
        criteria.add(Restrictions.eq("trans_id", transModel));
        return (List<TransactionDetailModel>) criteria.list();
    }
    
    
}
