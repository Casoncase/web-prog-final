package com.enterprise.dao;

import com.enterprise.models.CartModel;
import com.enterprise.models.UserModel;
import java.util.List;

public interface CartDao {
    void saveCart(CartModel cart);
    List<CartModel> findAllCart();
    void deleteCartById(int id);
    CartModel findById(int id);
    void updateCart(CartModel cart);
    CartModel findByUser(UserModel user);
}
