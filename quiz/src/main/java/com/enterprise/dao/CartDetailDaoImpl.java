package com.enterprise.dao;

import com.enterprise.models.CartDetailModel;
import com.enterprise.models.CartModel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("CartDetailDao")
public class CartDetailDaoImpl extends AbstractDao implements CartDetailDao{
    @Override
    @Transactional
    public void saveCartDetail(CartDetailModel cartDetail){
        persist(cartDetail);
    }

    @Override
    public List<CartDetailModel> findAllCartDetailOfUser(CartModel cart) {
        Criteria criteria = getSession().createCriteria(CartDetailModel.class);
        criteria.add(Restrictions.eq("cart", cart));
        return (List<CartDetailModel>) criteria.list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<CartDetailModel> findAllCartDetail(){
        Criteria criteria = getSession().createCriteria(CartDetailModel.class);
        return (List<CartDetailModel>) criteria.list();
    }
    
    @Override
    @Transactional
    public void deleteCartDetailById(int id){
        Query query = getSession().createSQLQuery("delete from cartdetails where cartdetail_id = :id");
        query.setString("id", String.valueOf(id));
        query.executeUpdate();
    }
    
    @Override
    public CartDetailModel findById(int id){
        Criteria criteria = getSession().createCriteria(CartDetailModel.class);
        criteria.add(Restrictions.eq("cartdetail_id", id));
        return (CartDetailModel) criteria.uniqueResult();
    }
    
    @Override
    @Transactional
    public void updateCartDetail(CartDetailModel cartDetail){
        getSession().update(cartDetail);
    }
}
