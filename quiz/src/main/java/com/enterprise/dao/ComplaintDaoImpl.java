/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.dao;

import com.enterprise.models.ComplaintModel;
import com.enterprise.models.TransactionDetailModel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("ComplaintDao")
public class ComplaintDaoImpl extends AbstractDao implements ComplaintDao{
    @Override
    @Transactional
    public void saveComplaint(ComplaintModel complaint){
        persist((ComplaintModel) complaint);
    }

    @Override
    public ComplaintModel findComplaintByTransDetail(TransactionDetailModel transDet) {
        Criteria criteria = getSession().createCriteria(ComplaintModel.class);
        criteria.add(Restrictions.eq("transactionDetail_id", transDet));
        return (ComplaintModel) criteria.uniqueResult();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<ComplaintModel> findAllComplaint(){
        System.out.println("masuk1");
        Criteria criteria = getSession().createCriteria(ComplaintModel.class);
        return (List<ComplaintModel>) criteria.list();
    }
    
    @Override
    @Transactional
    public void deleteComplaintById(int id){
        Query query = getSession().createSQLQuery("delete from complaints where complaint_id = :id");
        query.setString("id", String.valueOf(id));
        query.executeUpdate();
    }
    
    @Override
    public ComplaintModel findComplaintById(int id){
        Criteria criteria = getSession().createCriteria(ComplaintModel.class);
        criteria.add(Restrictions.eq("complaint_id", id));
        return (ComplaintModel) criteria.uniqueResult();
    }
    
    @Override
    @Transactional
    public void updateComplaint(ComplaintModel complaint){
        getSession().update(complaint);
    }
}
