package com.enterprise.dao;

import com.enterprise.models.ItemModel;
import com.enterprise.models.UOMConversionModel;
import com.enterprise.models.UOMListModel;
import java.util.List;

public interface UOMConversionDao {
    void saveUOMConversion(UOMConversionModel uomConversion);
    List<UOMConversionModel> findAllUOMConversion();
    void deleteUOMConversionById(int id);
    UOMConversionModel findUOMConversionById(int id);
    UOMConversionModel findByItemAndUom(ItemModel item, UOMListModel uom);
    void updateUOMConversion(UOMConversionModel uomConversion);
    public List<UOMConversionModel> findByItem(ItemModel item);
}
