package com.enterprise.dao;

import com.enterprise.models.CartModel;
import com.enterprise.models.UserModel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("CartDao")
public class CartDaoImpl extends AbstractDao implements CartDao{
    @Override
    @Transactional
    public void saveCart(CartModel cart){
        persist(cart);
    }
    
    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<CartModel> findAllCart(){
        Criteria criteria = getSession().createCriteria(CartModel.class);
        return (List<CartModel>) criteria.list();
    }
    
    @Override
    @Transactional
    public void deleteCartById(int id){
        Query query = getSession().createSQLQuery("delete from carts where cart_id = :id");
        query.setString("id", String.valueOf(id));
        query.executeUpdate();
    }
    
    @Override
    @Transactional
    public CartModel findById(int id){
        Criteria criteria = getSession().createCriteria(CartModel.class);
        criteria.add(Restrictions.eq("cart_id", id));
        return (CartModel) criteria.uniqueResult();
    }
    
    @Override
    @Transactional
    public void updateCart(CartModel cart){
        getSession().update(cart);
    }

    @Override
    @Transactional
    public CartModel findByUser(UserModel user) {
        Criteria criteria = getSession().createCriteria(CartModel.class);
        criteria.add(Restrictions.eq("user_id", user));
        return (CartModel) criteria.list().get(0);
    }
    
    
}
