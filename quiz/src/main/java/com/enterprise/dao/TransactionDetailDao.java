package com.enterprise.dao;

import com.enterprise.models.ItemModel;
import com.enterprise.models.TransactionDetailModel;
import com.enterprise.models.TransactionModel;
import java.util.List;

public interface TransactionDetailDao {
    void saveTransactionDetail(TransactionDetailModel transactionDetail);
    List<TransactionDetailModel> findAllTransactionDetail();
    void deleteTransactionDetailById(int id);
    TransactionDetailModel findTransactionDetailById(int id);
    void updateTransactionDetail(TransactionDetailModel transactionDetail);

    public List<TransactionDetailModel> findAllDetailsOfTransaction(TransactionModel transModel);

    public List<TransactionDetailModel> findTransactionDetailByItem(ItemModel item);
}
