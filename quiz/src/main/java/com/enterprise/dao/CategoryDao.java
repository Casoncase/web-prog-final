/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.dao;

import com.enterprise.models.CategoryModel;
import java.util.List;

public interface CategoryDao {
    void saveCategory(CategoryModel category);
    List<CategoryModel> findAllCategory();
    void deleteCategoryById(int id);
    CategoryModel findCategoryById(int id);
    List<CategoryModel> findCategoryByName(String name);
    void updateCategory(CategoryModel category);
}
