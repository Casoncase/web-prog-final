package com.enterprise.dao;

import com.enterprise.models.CategoryModel;
import com.enterprise.models.UOMListModel;
import java.util.List;

public interface UOMListDao {
    void saveUOMList(UOMListModel uomList);
    List<UOMListModel> findAllUOMList();
    List<UOMListModel> findByCategory(CategoryModel categoryModel);
    void deleteUOMListById(int id);
    UOMListModel findUOMListById(int id);
    void updateUOMList(UOMListModel uomList);
}
