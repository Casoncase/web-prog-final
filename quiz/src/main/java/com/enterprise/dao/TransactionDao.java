package com.enterprise.dao;

import com.enterprise.models.TransactionModel;
import com.enterprise.models.UserModel;
import java.util.List;

public interface TransactionDao {
    void saveTransaction(TransactionModel transaction);
    List<TransactionModel> findAllTransaction();
    void deleteTransactionById(int id);
    TransactionModel findById(int id);
    void updateTransaction(TransactionModel transactionDetail);
    public List<TransactionModel> findAllTransactionOfUser(UserModel user);
}
