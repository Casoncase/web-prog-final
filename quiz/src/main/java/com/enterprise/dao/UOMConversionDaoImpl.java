package com.enterprise.dao;

import com.enterprise.models.ItemModel;
import com.enterprise.models.TransactionModel;
import com.enterprise.models.UOMConversionModel;
import com.enterprise.models.UOMListModel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("UOMConversionDao")
public class UOMConversionDaoImpl extends AbstractDao implements UOMConversionDao{
    @Override
    @Transactional
    public void saveUOMConversion(com.enterprise.models.UOMConversionModel uomConversion){
        persist(uomConversion);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<UOMConversionModel> findAllUOMConversion(){
        Criteria criteria = getSession().createCriteria(UOMConversionModel.class);
        return (List<UOMConversionModel>) criteria.list();
    }
    
    @Override
    @Transactional
    public void deleteUOMConversionById(int id){
        Query query = getSession().createSQLQuery("delete from uomconversions where uomconversion_id = :id");
        query.setString("id", String.valueOf(id));
        query.executeUpdate();
    }
    
    @Override
    public UOMConversionModel findUOMConversionById(int id){
        Criteria criteria = getSession().createCriteria(UOMConversionModel.class);
        criteria.add(Restrictions.eq("uomconversion_id", id));
        return (UOMConversionModel) criteria.uniqueResult();
    }
    
    @Override
    @Transactional
    public void updateUOMConversion (com.enterprise.models.UOMConversionModel transaction){
        getSession().update(transaction);
    }

    @Override
    public UOMConversionModel findByItemAndUom(ItemModel item, UOMListModel uom) {
        Criteria criteria = getSession().createCriteria(UOMConversionModel.class);
        criteria.add(Restrictions.eq("item_id", item));
        criteria.add(Restrictions.eq("uom_id", uom));
        return (UOMConversionModel) criteria.uniqueResult();
    }

    @Override
    public List<UOMConversionModel> findByItem(ItemModel item) {
        Criteria criteria = getSession().createCriteria(UOMConversionModel.class);
        criteria.add(Restrictions.eq("item_id", item));
        return (List<UOMConversionModel>) criteria.list();
    }
}
