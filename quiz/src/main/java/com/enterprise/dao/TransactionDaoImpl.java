package com.enterprise.dao;

import com.enterprise.models.TransactionModel;
import com.enterprise.models.UserModel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("TransactionDao")
public class TransactionDaoImpl extends AbstractDao implements TransactionDao{
    @Override
    @Transactional
    public void saveTransaction(TransactionModel transaction){
        persist(transaction);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<TransactionModel> findAllTransaction(){
        Criteria criteria = getSession().createCriteria(TransactionModel.class);
        return (List<TransactionModel>) criteria.list();
    }
    
    @Override
    @Transactional
    public void deleteTransactionById(int id){
        Query query = getSession().createSQLQuery("delete from transactions where trans_id = :id");
        query.setString("id", String.valueOf(id));
        query.executeUpdate();
    }
    
    @Override
    public TransactionModel findById(int id){
        Criteria criteria = getSession().createCriteria(TransactionModel.class);
        criteria.add(Restrictions.eq("trans_id", id));
        return (TransactionModel) criteria.uniqueResult();
    }
    
    @Override
    @Transactional
    public void updateTransaction (TransactionModel transaction){
        getSession().update(transaction);
    }

    @Override
    public List<TransactionModel> findAllTransactionOfUser(UserModel user) {
        Criteria criteria = getSession().createCriteria(TransactionModel.class);
        criteria.add(Restrictions.eq("user_id", user));
        return (List<TransactionModel>) criteria.list();
    }
}
