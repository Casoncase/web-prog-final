/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.dao;

import com.enterprise.models.CategoryModel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("CategoryDao")
public class CategoryDaoImpl extends AbstractDao implements CategoryDao{
    @Override
    @Transactional
    public void saveCategory(CategoryModel category){
        persist((CategoryModel) category);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<CategoryModel> findAllCategory(){
        System.out.println("masuk1");
        Criteria criteria = getSession().createCriteria(CategoryModel.class);
        return (List<CategoryModel>) criteria.list();
    }
    
    @Override
    @Transactional
    public void deleteCategoryById(int id){
        Query query = getSession().createSQLQuery("delete from categories where category_id = :id");
        query.setString("id", String.valueOf(id));
        query.executeUpdate();
    }
    
    @Override
    public CategoryModel findCategoryById(int id){
        Criteria criteria = getSession().createCriteria(CategoryModel.class);
        criteria.add(Restrictions.eq("category_id", id));
        return (CategoryModel) criteria.uniqueResult();
    }
    
    @Override
    public List<CategoryModel> findCategoryByName(String name){
        Criteria criteria = getSession().createCriteria(CategoryModel.class);
        criteria.add(Restrictions.like("category", name, MatchMode.ANYWHERE));
        return (List<CategoryModel>) criteria.list();
    }
    
    @Override
    @Transactional
    public void updateCategory(CategoryModel category){
        getSession().update(category);
    }
}
