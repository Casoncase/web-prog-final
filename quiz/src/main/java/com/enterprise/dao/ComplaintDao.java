/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.dao;

import com.enterprise.models.ComplaintModel;
import com.enterprise.models.TransactionDetailModel;
import java.util.List;

/**
 *
 * @author Cason Kang
 */
public interface ComplaintDao {
    void saveComplaint(ComplaintModel complaint);
    List<ComplaintModel> findAllComplaint();
    void deleteComplaintById(int id);
    ComplaintModel findComplaintById(int id);
    void updateComplaint(ComplaintModel complaint);
    public ComplaintModel findComplaintByTransDetail(TransactionDetailModel transDet);
}
