/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.dao;

import com.enterprise.models.CategoryModel;
import com.enterprise.models.ItemModel;
import com.enterprise.models.UserModel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("ItemDao")
public class ItemDaoImpl extends AbstractDao implements ItemDao{
    @Override
    @Transactional
    public void saveItem(ItemModel item){
        persist((ItemModel) item);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<ItemModel> findAllItem(){
        System.out.println("masuk1");
        Criteria criteria = getSession().createCriteria(ItemModel.class);
        return (List<ItemModel>) criteria.list();
    }
    
    @Override
    @Transactional
    public void deleteItemById(int id){
        Query query = getSession().createSQLQuery("delete from items where item_id = :id");
        query.setString("id", String.valueOf(id));
        query.executeUpdate();
    }
    
    @Override
    public ItemModel findItemById(int id){
        Criteria criteria = getSession().createCriteria(ItemModel.class);
        criteria.add(Restrictions.eq("item_id", id));
        return (ItemModel) criteria.uniqueResult();
    }
    
    @Override
    public List<ItemModel> findItemByName(String name){
        Criteria criteria = getSession().createCriteria(ItemModel.class);
        criteria.add(Restrictions.like("item_name", name, MatchMode.ANYWHERE));
        return (List<ItemModel>) criteria.list();
    }
    
    @Override
    @Transactional
    public void updateItem(ItemModel item){
        getSession().update(item);
    }

    @Override
    public List<ItemModel> findItemByCategory(CategoryModel category) {
        Criteria criteria = getSession().createCriteria(ItemModel.class);
        criteria.add(Restrictions.eq("category_id", category));
        return (List<ItemModel>) criteria.list();
    }

    @Override
    public List<ItemModel> findItemByUser(UserModel user) {
        Criteria criteria = getSession().createCriteria(ItemModel.class);
        criteria.add(Restrictions.eq("user_id", user));
        return (List<ItemModel>) criteria.list();
    }
}
