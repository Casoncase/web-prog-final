/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.dao;

import com.enterprise.models.CategoryModel;
import com.enterprise.models.ItemModel;
import com.enterprise.models.UserModel;
import java.util.List;

public interface ItemDao {
    void saveItem(ItemModel item);
    List<ItemModel> findAllItem();
    void deleteItemById(int id);
    ItemModel findItemById(int id);
    List<ItemModel> findItemByName(String name);
    void updateItem(ItemModel item);
    List<ItemModel> findItemByCategory(CategoryModel category);
    List<ItemModel> findItemByUser(UserModel user);
}
