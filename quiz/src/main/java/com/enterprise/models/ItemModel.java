/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="\"items\"")
public class ItemModel implements Serializable{
    
    @Id
    @Column(name = "item_id")
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int item_id;
    
    @Column(name = "item_name", nullable = false)
    private String item_name;
    
    @OneToOne
    @JoinColumn(name = "category_id", nullable = false)
    private CategoryModel category_id;
    
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserModel user_id;
    
    @Column(name = "img_url")
    private String img_url;

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public CategoryModel getCategory_id() {
        return category_id;
    }

    public UserModel getUser_id() {
        return user_id;
    }

    public void setUser_id(UserModel user_id) {
        this.user_id = user_id;
    }

    public void setCategory_id(CategoryModel category_id) {
        this.category_id = category_id;
    }
    
    

    
    
    
    
   
    
     
}
