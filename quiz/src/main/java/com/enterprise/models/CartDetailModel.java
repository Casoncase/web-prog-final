/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="\"cartdetails\"")
public class CartDetailModel implements Serializable{

    @Id
    @Column(name = "cartdetail_id")
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int cartdetail_id;
    
    @ManyToOne
    @JoinColumn(name = "cart_id", nullable = false)
    private CartModel cart;
    
    @ManyToOne
    @JoinColumn(name = "item_id", nullable = false)
    private ItemModel item;
    
    @ManyToOne
    @JoinColumn(name = "uom_id", nullable = false)
    private UOMConversionModel uomconversion_id;

    public UOMConversionModel getUomconversion_id() {
        return uomconversion_id;
    }

    public void setUomconversion_id(UOMConversionModel uomconversion_id) {
        this.uomconversion_id = uomconversion_id;
    }

    @Column(name = "quantity")
    private int quantity;

    public int getCartdetail_id() {
        return cartdetail_id;
    }

    public void setCartdetail_id(int cartdetail_id) {
        this.cartdetail_id = cartdetail_id;
    }

    public CartModel getCart() {
        return cart;
    }

    public void setCart(CartModel cart) {
        this.cart = cart;
    }

    public ItemModel getItem() {
        return item;
    }

    public void setItem(ItemModel item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    
}
