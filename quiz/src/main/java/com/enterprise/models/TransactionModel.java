/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.models;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="\"transactions\"")
public class TransactionModel implements Serializable{
    
    @Id
    @Column(name = "trans_id")
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int trans_id;
    
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserModel user_id;
    
    @Column(name = "status", nullable = false)
    private int status;
    
    @Column(name = "date", nullable = false)
    private Date date;

    public int getTrans_id() {
        return trans_id;
    }

    public UserModel getUser_id() {
        return user_id;
    }

    public void setUser_id(UserModel user_id) {
        this.user_id = user_id;
    }

    public void setTrans_id(int trans_id) {
        this.trans_id = trans_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }    
}
