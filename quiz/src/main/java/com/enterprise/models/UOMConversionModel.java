/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Cason Kang
 */

@Entity
@Table(name="\"uomconversions\"")
public class UOMConversionModel implements Serializable{
    
    @Id
    @Column(name = "uomconversion_id")
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int uomconversion_id;
    
    @ManyToOne
    @JoinColumn(name = "uom_id", nullable = false)
    private UOMListModel uom_id;
    
    @ManyToOne
    @JoinColumn(name = "item_id", nullable = false)
    private ItemModel item_id;
    
    @Column(name = "price", nullable = false)
    private float price;
    
    @Column(name = "stock", nullable = false)
    private int stock;

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
    
    public int getUomconversion_id() {
        return uomconversion_id;
    }

    public void setUomconversion_id(int uomconversion_id) {
        this.uomconversion_id = uomconversion_id;
    }

    public UOMListModel getUom_id() {
        return uom_id;
    }

    public void setUom_id(UOMListModel uom_id) {
        this.uom_id = uom_id;
    }

    public ItemModel getItem_id() {
        return item_id;
    }

    public void setItem_id(ItemModel item_id) {
        this.item_id = item_id;
    }
    

    
    
    
}
