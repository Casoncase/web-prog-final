/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="\"uomlist\"")
public class UOMListModel implements Serializable{
    
    @Id
    @Column(name = "uom_id")
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int uom_id;
    
    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false)
    private CategoryModel category;
    
    @Column(name = "uom_name", nullable = false)
    private String uom_name;

    public int getUom_id() {
        return uom_id;
    }

    public void setUom_id(int uom_id) {
        this.uom_id = uom_id;
    }
    
    public String getUom_name() {
        return uom_name;
    }

    public void setUom_name(String uom_name) {
        this.uom_name = uom_name;
    }
    
    
}
