/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Cason Kang
 */
@Entity
@Table(name="\"carts\"")
public class CartModel implements Serializable {
    
    @Id
    @Column(name = "cart_id")
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int cart_id;
    
    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserModel user_id;

    public int getCart_id() {
        return cart_id;
    }

    public void setCart_id(int cart_id) {
        this.cart_id = cart_id;
    }

   
    public UserModel getUser_id() {
        return user_id;
    }

    public void setUser_id(UserModel user_id) {
        this.user_id = user_id;
    }

    
    
    
}
