/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="\"transactiondetail\"")
public class TransactionDetailModel implements Serializable{
    
    @Id
    @Column(name = "transdetail_id")
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int transdetail_id;
    
    @ManyToOne
    @JoinColumn(name = "trans_id", nullable = false)
    private TransactionModel trans_id;
    
    @ManyToOne
    @JoinColumn(name = "item_id", nullable = false)
    private ItemModel item_id;
    
    @Column(name = "qty", nullable = false)
    private int qty;
    
    @Column(name = "price", nullable = false)
    private float price;
    
    @ManyToOne
    @JoinColumn(name = "uom_id", nullable = false)
    private UOMListModel uom_id;

    public int getTransactiondetail_id() {
        return transdetail_id;
    }

    public void setTransactiondetail_id(int transactiondetail_id) {
        this.transdetail_id = transactiondetail_id;
    }

    public TransactionModel getTrans_id() {
        return trans_id;
    }

    public void setTrans_id(TransactionModel trans_id) {
        this.trans_id = trans_id;
    }

    public ItemModel getItem_id() {
        return item_id;
    }

    public void setItem_id(ItemModel item_id) {
        this.item_id = item_id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public UOMListModel getUom_id() {
        return uom_id;
    }

    public void setUom_id(UOMListModel uom_id) {
        this.uom_id = uom_id;
    }
    
    
}
