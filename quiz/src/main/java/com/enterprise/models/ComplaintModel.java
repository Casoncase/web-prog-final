/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.models;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="\"complaints\"")
public class ComplaintModel implements Serializable{
    
    @Id
    @Column(name = "complaint_id")
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int complaint_id;
    
    @Column(name = "date", nullable = false)
    private Date date;
    
    @ManyToOne
    @JoinColumn(name = "issuer_id", nullable = false)
    private UserModel issuer_id;
    
    @ManyToOne
    @JoinColumn(name = "transactionDetail_id", nullable = false)
    private TransactionDetailModel transactionDetail_id;
    
    @Column(name = "content", nullable = false)
    private String content;

    public int getComplaint_id() {
        return complaint_id;
    }

    public void setComplaint_id(int complaint_id) {
        this.complaint_id = complaint_id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public UserModel getIssuer_id() {
        return issuer_id;
    }

    public void setIssuer_id(UserModel issuer_id) {
        this.issuer_id = issuer_id;
    }

    public TransactionDetailModel getTransactionDetail_id() {
        return transactionDetail_id;
    }

    public void setTransactionDetail_id(TransactionDetailModel transactionDetail_id) {
        this.transactionDetail_id = transactionDetail_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
    
}
