package com.enterprise.controllers;

import com.enterprise.dao.CartDao;
import com.enterprise.models.CartModel;
import com.enterprise.models.TokenModel;
import com.enterprise.models.UserModel;
import com.enterprise.services.CartServices;
import com.enterprise.services.CryptWithSHA256;
import com.enterprise.services.TokenServices;
import com.enterprise.services.UserServices;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Controller
@ComponentScan("com.enterprise.services")
public class UserController {
    
    @Autowired
    private UserServices userServices;
    @Autowired
    private CartServices cartServices;
    @Autowired
    private TokenServices tokenServices;
    
    //login
    @RequestMapping (value="/login", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TokenModel> login(@RequestBody Map<String, Object> data){
        UserModel userWithEmail = (UserModel) userServices.findByEmail(data.get("email").toString());
        String enteredPassword = data.get("password").toString();
        TokenModel token = tokenServices.findByUser(userWithEmail);
        Timestamp time = new Timestamp(System.currentTimeMillis());
        HttpHeaders headers = new HttpHeaders();
        if(userWithEmail == null){
            return new ResponseEntity<TokenModel> (headers, HttpStatus.NO_CONTENT);
        }else{
            if(userWithEmail.getPassword().equals(CryptWithSHA256.cryptWithSHA256(enteredPassword))){
                if(token == null){
                    token = new TokenModel();
                    System.out.println(userWithEmail.getEmail());
                    token.setUser(userWithEmail);
                    token.setCreated_at(time);
                    token.setToken(CryptWithSHA256.cryptWithSHA256(String.valueOf(System.currentTimeMillis())));
                    tokenServices.saveToken(token);
                }else{
                    if((time.getTime() - token.getCreated_at().getTime()) > 10800000){
                        System.out.println("Token expired, creating new one");
                        tokenServices.deleteTokensById(token.getToken_id());
                        token = new TokenModel();
                        token.setUser(userWithEmail);
                        token.setCreated_at(time);
                        token.setToken(CryptWithSHA256.cryptWithSHA256(String.valueOf(System.currentTimeMillis())));
                        tokenServices.saveToken(token);
                    }
                }
                return new ResponseEntity<TokenModel> (token, HttpStatus.OK);
            }else{
                return new ResponseEntity<TokenModel> (headers, HttpStatus.FORBIDDEN);
            }
        }
    }
    
    /* get all User */
    @RequestMapping (value="/user", method=RequestMethod.GET)
    public ResponseEntity<List<UserModel>> listUsers(ModelMap models){
        List<UserModel> users = userServices.findAllUser();
        if(users.isEmpty()){
            return new ResponseEntity<List<UserModel>> (HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<UserModel>> (users, HttpStatus.OK);
    }
    
    /* get single User by ID*/
    @RequestMapping (value="/user/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserModel> getUserById(@PathVariable("id") String id){
        UserModel user = (UserModel) userServices.findById(Integer.parseInt(id));
        if(user == null){
            System.out.println("User with ID: " + id + " not found");
            return new ResponseEntity<UserModel>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<UserModel> (user, HttpStatus.OK);
    }
    
    /* add / register */
    @RequestMapping (value="/user", method=RequestMethod.POST)
    public ResponseEntity<Void> createUser(@RequestBody UserModel user, UriComponentsBuilder ucBuilder){
        System.out.println("Creating User " + user.getName());
        HttpHeaders headers = new HttpHeaders();
        System.out.println(CryptWithSHA256.cryptWithSHA256(user.getPassword()));
        user.setPassword(CryptWithSHA256.cryptWithSHA256(user.getPassword()));
        UserModel checkUser = new UserModel();
        checkUser = userServices.findByEmail(user.getEmail());
        if(checkUser != null){
            System.out.println(checkUser.getName());
            return new ResponseEntity<Void> (headers, HttpStatus.CONFLICT);
        }
        userServices.saveUser(user);
        System.out.println(user.getUser_id());
        
        CartModel cart = new CartModel();
        cart.setUser_id(user);
        cartServices.saveCart(cart);
        
        return new ResponseEntity<Void> (headers, HttpStatus.CREATED);
    }
    
    /* update */
    @RequestMapping (value="/user/{id}", method=RequestMethod.PUT)
    public ResponseEntity<UserModel> updateUser(@PathVariable("id") String id, @RequestBody UserModel user){
        System.out.println("Updating User " + id);
        UserModel currentUser = (UserModel) userServices.findById(Integer.parseInt(id));
        if(currentUser == null){
            System.out.println("User with id: +" + id + " not found");
            return new ResponseEntity<UserModel> (HttpStatus.NOT_FOUND);
        }
        currentUser.setName(user.getName());
        userServices.updateUser(currentUser);
        return new ResponseEntity<UserModel>(currentUser, HttpStatus.OK);
    }
    
    /* delete */
    @RequestMapping (value="/user/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<UserModel> removeUser(@PathVariable("id") String id){
        UserModel user = (UserModel) userServices.findById(Integer.parseInt(id));
        if(user == null){
            System.out.println("Unable to delete. User with id" + id + " not found");
            return new ResponseEntity<UserModel>(HttpStatus.NOT_FOUND);
        }
        userServices.deleteUserById(Integer.parseInt(id));
        return new ResponseEntity<UserModel>(HttpStatus.NO_CONTENT);
    }
    
//    change password
    @RequestMapping (value="/user-password/{id}", method=RequestMethod.PUT)
    public ResponseEntity<UserModel> updatePassword(@PathVariable("id") String id, @RequestBody Map<String, Object> data){
        System.out.println("Updating User " + id);
        UserModel currentUser = (UserModel) userServices.findById(Integer.parseInt(id));
        if(currentUser == null){
            System.out.println("User with id: +" + id + " not found");
            return new ResponseEntity<UserModel> (HttpStatus.NOT_FOUND);
        }
        else if(currentUser.getPassword().equals(CryptWithSHA256.cryptWithSHA256(data.get("oldPassword").toString()))){
            if(data.get("newPassword").equals(data.get("confirmNewPassword"))){ 
                currentUser.setPassword(CryptWithSHA256.cryptWithSHA256(data.get("newPassword").toString()));
                userServices.updateUser(currentUser);
                return new ResponseEntity<UserModel>(currentUser, HttpStatus.OK);
                }
                System.out.println("Old Password do no match");
                return new ResponseEntity<UserModel> (HttpStatus.NOT_ACCEPTABLE);
        }
                System.out.println("New password do not match");
                return new ResponseEntity<UserModel> (HttpStatus.NOT_ACCEPTABLE);
    }
}