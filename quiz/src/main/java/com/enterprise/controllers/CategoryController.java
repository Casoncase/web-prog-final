/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.controllers;

import com.enterprise.models.CategoryModel;
import com.enterprise.services.CategoryServices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Cason Kang
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Controller
@ComponentScan("com.enterprise.services")
public class CategoryController {
    
    @Autowired
    private CategoryServices categoryServices;
    
    @RequestMapping(value="/category", method=RequestMethod.GET)
    public ResponseEntity<List<CategoryModel>> listCategories(ModelMap models) {
        List<CategoryModel> categories = categoryServices.findAllCategory();
        if(categories.isEmpty()){
            return new ResponseEntity<List<CategoryModel>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<CategoryModel>>(categories, HttpStatus.OK);
    }
//    get single category
    @RequestMapping(value="/category/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CategoryModel> getCategory(@PathVariable("id") String id) {
        CategoryModel category = categoryServices.findCategoryById(Integer.parseInt(id));
        if(category==null) {
            System.out.println("Category with id" + id + " not found");
            return new ResponseEntity<CategoryModel>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<CategoryModel>(category, HttpStatus.OK);
    }
    
//    add category
    @RequestMapping(value="/category", method=RequestMethod.POST)
    public ResponseEntity<Void> createCategory(@RequestBody CategoryModel category, UriComponentsBuilder ucBuilder) {
        System.out.println("Creating Category " + category.getCategory());
        categoryServices.saveCategory(category);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
//    update
    @RequestMapping(value = "/category/{id}", method = RequestMethod.PUT)
    public ResponseEntity<CategoryModel> updateCategory(@PathVariable("id") String id, @RequestBody CategoryModel category) {
        System.out.println("Updating category " + id);
        CategoryModel currentCategory = categoryServices.findCategoryById(Integer.parseInt(id));
        if (currentCategory==null) {
            System.out.println("Category with id" + id + " not found");
            return new ResponseEntity<CategoryModel>(HttpStatus.NOT_FOUND);
        }
        currentCategory.setCategory(category.getCategory());
        categoryServices.updateCategory(currentCategory);
        return new ResponseEntity<CategoryModel>(currentCategory, HttpStatus.OK);
    }
    
//    delete
    @RequestMapping(value="/category/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<CategoryModel> removeCategory(@PathVariable("id") String id) {
        
        CategoryModel category = categoryServices.findCategoryById(Integer.parseInt(id));
        if(category == null) {
            System.out.println("Unable to delete. Category with id " + id + " not found");
            return new ResponseEntity<CategoryModel>(HttpStatus.NOT_FOUND);
        }
        categoryServices.deleteCategoryById(Integer.parseInt(id));
        return new ResponseEntity<CategoryModel>(HttpStatus.NO_CONTENT);
    }
    
//    search by name
    @RequestMapping(value="/category-getbyname/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CategoryModel>> getCategoryName(@PathVariable("name") String name) {
        List<CategoryModel> category = categoryServices.findCategoryByName(name);
        if(category.isEmpty()) {
            System.out.println("Category with name " + name + " not found");
            return new ResponseEntity<List<CategoryModel>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<CategoryModel>>(category, HttpStatus.OK);
    }
}
