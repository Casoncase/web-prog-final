/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.controllers;

import com.enterprise.models.CategoryModel;
import com.enterprise.models.UOMListModel;
import com.enterprise.services.CategoryServices;
import com.enterprise.services.UOMListServices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Cason Kang
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Controller
@ComponentScan("com.enterprise.services")
public class UOMController {
    
    @Autowired
    private UOMListServices uomListServices;
    @Autowired
    private CategoryServices categoryServices;
    
    @RequestMapping(value="/uom_list", method=RequestMethod.GET)
    public ResponseEntity<List<UOMListModel>> listUOMList(ModelMap models) {
        List<UOMListModel> categories = uomListServices.findAllUOMList();
        if(categories.isEmpty()){
            return new ResponseEntity<List<UOMListModel>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<UOMListModel>>(categories, HttpStatus.OK);
    }
//    get single uom
    @RequestMapping(value="/uom_list/id/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UOMListModel> getUOMList(@PathVariable("id") String id) {
        UOMListModel category = uomListServices.findUOMListById(Integer.parseInt(id));
        if(category==null) {
            System.out.println("User with id" + id + " not found");
            return new ResponseEntity<UOMListModel>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<UOMListModel>(category, HttpStatus.OK);
    }
    
//    add uom
    @RequestMapping(value="/uom_list", method=RequestMethod.POST)
    public ResponseEntity<Void> createUOMList(@RequestBody UOMListModel uomList, UriComponentsBuilder ucBuilder) {
        System.out.println("Creating UOM " + uomList.getUom_name());
        uomListServices.saveUOMList(uomList);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
//    update
    @RequestMapping(value = "/uom_list/{id}", method = RequestMethod.PUT)
    public ResponseEntity<UOMListModel> updateUOMList(@PathVariable("id") String id, @RequestBody UOMListModel category) {
        System.out.println("Updating category " + id);
        UOMListModel currUomList = uomListServices.findUOMListById(Integer.parseInt(id));
        if (currUomList==null) {
            System.out.println("Category with id" + id + " not found");
            return new ResponseEntity<UOMListModel>(HttpStatus.NOT_FOUND);
        }
        currUomList.setUom_name(category.getUom_name());
        uomListServices.updateUOMList(currUomList);
        return new ResponseEntity<UOMListModel>(currUomList, HttpStatus.OK);
    }
    
//    delete
    @RequestMapping(value="/uom_list/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<UOMListModel> removeUOMList(@PathVariable("id") String id) {
        
        UOMListModel uomList = uomListServices.findUOMListById(Integer.parseInt(id));
        if(uomList == null) {
            System.out.println("Unable to delete. Category with id " + id + " not found");
            return new ResponseEntity<UOMListModel>(HttpStatus.NOT_FOUND);
        }
        uomListServices.deleteUOMListById(Integer.parseInt(id));
        return new ResponseEntity<UOMListModel>(HttpStatus.NO_CONTENT);
    }
    
    //find uom list by category
    @RequestMapping(value="/uom_list/{category}", method=RequestMethod.GET)
    public ResponseEntity<List<UOMListModel>> findUOMListByCategory(@PathVariable("category") String categoryName) {
        CategoryModel category = categoryServices.findSingleCategoryByName(categoryName);
        List<UOMListModel> uomList = uomListServices.findByCategory(category);
        if(uomList == null) {
            System.out.println("Unable to find uom with category " + category.getCategory() + " not found");
            return new ResponseEntity<List<UOMListModel>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<UOMListModel>>(uomList, HttpStatus.OK);
    }
}
