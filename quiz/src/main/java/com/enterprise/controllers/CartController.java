/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.controllers;

import com.enterprise.models.CartDetailModel;
import com.enterprise.models.CartModel;
import com.enterprise.models.ItemModel;
import com.enterprise.models.TokenModel;
import com.enterprise.models.UOMConversionModel;
import com.enterprise.models.UOMListModel;
import com.enterprise.models.UserModel;
import com.enterprise.services.CartDetailServices;
import com.enterprise.services.CartServices;
import com.enterprise.services.ItemServices;
import com.enterprise.services.TokenServices;
import com.enterprise.services.UOMConversionServices;
import com.enterprise.services.UOMListServices;
import com.enterprise.services.UserServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Cason Kang
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Controller
@ComponentScan("com.enterprise.services")
public class CartController {
    
    @Autowired
    private ItemServices itemServices;
    @Autowired
    private UserServices userServices;
    @Autowired
    private UOMListServices uomServices;
    @Autowired
    private UOMConversionServices uomConvServices;
    @Autowired
    private CartServices cartServices;
    @Autowired
    private CartDetailServices cartDetailServices;
    @Autowired
    private TokenServices tokenServices;
    
    //Add item to user cart
    //int user_id
    //int item_id
    //int uom_id
    //int qty
    //string token
    @RequestMapping(value = "/cart/add", method = RequestMethod.POST)
    public ResponseEntity<Void> addItemToCart(@RequestBody Map<String, Object> data, UriComponentsBuilder ucBuilder) {
        UserModel user = userServices.findById(Integer.parseInt(data.get("user_id").toString()));
        TokenModel token = tokenServices.findByUser(user);
        if((System.currentTimeMillis() - token.getCreated_at().getTime()) > 10800000){
            System.out.println("Token expired");
            tokenServices.deleteTokensById(token.getToken_id());
            return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
        }
        CartModel cart = new CartModel();
        cart = cartServices.findByUser(user);
        ItemModel item = itemServices.findItemById(Integer.parseInt(data.get("item_id").toString()));
        UOMListModel uom = uomServices.findUOMListById(Integer.parseInt(data.get("uom_id").toString()));
        UOMConversionModel uomConv = uomConvServices.findByItemAndUom(item, uom);
        
        if(cart == null || item == null || uomConv == null) return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        List<CartDetailModel> cartDetails = cartDetailServices.findAllCartDetailOfUser(cart);

        for(int i = 0; i < cartDetails.size(); i++){
            if(cartDetails.get(i).getItem().getItem_id() == item.getItem_id()) return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
            System.out.println(cartDetails.get(i).getItem().getItem_name());
        }
        
        CartDetailModel cartDetail = new CartDetailModel();
        cartDetail.setCart(cart);
        cartDetail.setItem(item);
        cartDetail.setQuantity(Integer.parseInt(data.get("qty").toString()));
        cartDetail.setUomconversion_id(uomConv);
        
        cartDetailServices.saveCartDetail(cartDetail);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    //edit quantity item in user cart
    //int qty
    //string token
    @RequestMapping(value = "/cart/{user_id}/edit/{cart_detail_id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> editItemInCart(@PathVariable("user_id") String user_id, @PathVariable("cart_detail_id") String cart_detail_id, @RequestBody Map<String, Object> data, UriComponentsBuilder ucBuilder) {
        TokenModel token = tokenServices.findByUser(userServices.findById(Integer.parseInt(user_id)));
        CartDetailModel cartDetail = cartDetailServices.findById(Integer.parseInt(cart_detail_id));
        if((System.currentTimeMillis() - token.getCreated_at().getTime()) > 10800000 && token.getToken().equals(data.get("token").toString())){
            System.out.println("Token expired");
            tokenServices.deleteTokensById(token.getToken_id());
            return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
        }
        CartModel cart = new CartModel();
        UserModel user = userServices.findById(Integer.parseInt(user_id));
        cart = cartServices.findByUser(user);
        
        cartDetail = cartDetailServices.findById(Integer.parseInt(cart_detail_id));
        cartDetail.setQuantity(Integer.parseInt(data.get("qty").toString()));
        
        cartDetailServices.saveCartDetail(cartDetail);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    //delete item from user cart
    @RequestMapping(value = "/cart/{user_id}/delete/{cart_detail_id}", method = RequestMethod.POST)
    public ResponseEntity<Void> deleteItemInCart(@PathVariable("user_id") String user_id, @PathVariable("cart_detail_id") String cart_detail_id, @RequestBody Map<String, Object> data, UriComponentsBuilder ucBuilder) {
        TokenModel token = tokenServices.findByUser(userServices.findById(Integer.parseInt(user_id)));
        if((System.currentTimeMillis() - token.getCreated_at().getTime()) > 10800000 && token.getToken().equals(data.get("token").toString())){
            System.out.println("Token expired");
            tokenServices.deleteTokensById(token.getToken_id());
            return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
        }
        cartDetailServices.deleteCartDetailById(Integer.parseInt(cart_detail_id));
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    //list detail of user cart
    @RequestMapping(value = "/cart/{user_id}", method = RequestMethod.GET)
    public ResponseEntity<List<CartDetailModel>> viewItemsInCart(@PathVariable("user_id") String user_id, UriComponentsBuilder ucBuilder) {
        CartModel cart = new CartModel();
        UserModel user = userServices.findById(Integer.parseInt(user_id));
        cart = cartServices.findByUser(user);
        List<CartDetailModel> cartDetails = cartDetailServices.findAllCartDetailOfUser(cart);
        if(cartDetails.isEmpty()) return new ResponseEntity<List<CartDetailModel>> (HttpStatus.NOT_FOUND);
        return new ResponseEntity<List<CartDetailModel>>(cartDetails, HttpStatus.OK);
    }
    
    //get all carts
    @RequestMapping(value="/cart", method=RequestMethod.GET)
    public ResponseEntity<List<CartModel>> listItems(ModelMap models) {
        List<CartModel> items = cartServices.findAllCart();
        if(items.isEmpty()){
            return new ResponseEntity<List<CartModel>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<CartModel>>(items, HttpStatus.OK);
    }
//    get single cart
    @RequestMapping(value="/cart/id/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CartModel> getItem(@PathVariable("id") String id) {
        CartModel transaction = cartServices.findById(Integer.parseInt(id));
        if(transaction==null) {
            System.out.println("Cart with id" + id + " not found");
            return new ResponseEntity<CartModel>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<CartModel>(transaction, HttpStatus.OK);
    }
    
//    add cart
    @RequestMapping(value="/cart", method=RequestMethod.POST)
    public ResponseEntity<Void> createItem(@RequestBody Map<String, Object> data, UriComponentsBuilder ucBuilder) {
//      System.out.println("Creating Cart " + cart.getCart_id());
        CartModel cart = new CartModel();
        int user_id = Integer.parseInt(data.get("user_id").toString());
        UserModel user = userServices.findById(user_id);
        System.out.println(user.getEmail());
        cart.setUser_id(user);
        cartServices.saveCart(cart);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
//    update
    @RequestMapping(value = "/cart/{id}", method = RequestMethod.PUT)
    public ResponseEntity<CartModel> updateItem(@PathVariable("id") String id, @RequestBody Map<String, Object> data) {
        System.out.println("Updating item " + id);
        CartModel currCart = cartServices.findById(Integer.parseInt(id));
        if (currCart==null) {
            System.out.println("Item with id" + id + " not found");
            return new ResponseEntity<CartModel>(HttpStatus.NOT_FOUND);
        }
        cartServices.updateCart(currCart);
        return new ResponseEntity<CartModel>(currCart, HttpStatus.OK);
    }
    
//    delete
    @RequestMapping(value="/cart/id/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<CartModel> removeItem(@PathVariable("id") String id) {
        
        CartModel cart = cartServices.findById(Integer.parseInt(id));
        if(cart == null) {
            System.out.println("Unable to delete. Item with id " + id + " not found");
            return new ResponseEntity<CartModel>(HttpStatus.NOT_FOUND);
        }
        cartServices.deleteCartById(Integer.parseInt(id));
        return new ResponseEntity<CartModel>(HttpStatus.NO_CONTENT);
    }
}
