/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.controllers;

import com.enterprise.models.CartDetailModel;
import com.enterprise.models.CartModel;
import com.enterprise.models.TokenModel;
import com.enterprise.models.TransactionDetailModel;
import com.enterprise.models.TransactionModel;
import com.enterprise.models.UOMConversionModel;
import com.enterprise.models.UserModel;
import com.enterprise.services.CartDetailServices;
import com.enterprise.services.CartServices;
import com.enterprise.services.CategoryServices;
import com.enterprise.services.ItemServices;
import com.enterprise.services.TokenServices;
import com.enterprise.services.TransactionDetailServices;
import com.enterprise.services.TransactionServices;
import com.enterprise.services.UOMConversionServices;
import com.enterprise.services.UOMListServices;
import com.enterprise.services.UserServices;
import java.sql.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Cason Kang
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Controller
@ComponentScan("com.enterprise.services")
public class TransactionController {
    
    @Autowired
    private ItemServices itemServices;
    @Autowired
    private UserServices userServices;
    @Autowired
    private CategoryServices catServices;
    @Autowired
    private UOMListServices uomServices;
    @Autowired
    private UOMConversionServices uomConvServices;
    @Autowired
    private CartServices cartServices;
    @Autowired
    private CartDetailServices cartDetailServices;
    @Autowired
    private TransactionServices transactionServices;
    @Autowired
    private TransactionDetailServices transactionDetailServices;
    @Autowired
    private TokenServices tokenServices;
    //get all transactions
    @RequestMapping(value="/transaction", method=RequestMethod.GET)
    public ResponseEntity<List<TransactionModel>> listTransactions(ModelMap models) {
        List<TransactionModel> items = transactionServices.findAllTransaction();
        if(items.isEmpty()){
            return new ResponseEntity<List<TransactionModel>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<TransactionModel>>(items, HttpStatus.OK);
    }
    
    //get all transactiondetails
    @RequestMapping(value="/transaction/details/{trans_id}", method=RequestMethod.GET)
    public ResponseEntity<List<TransactionDetailModel>> listTransactionDetails(@PathVariable("id") String trans_id) {
        List<TransactionDetailModel> transactions = transactionDetailServices.findAllDetailsOfTransaction(transactionServices.findById(Integer.parseInt(trans_id)));
        if(transactions.isEmpty()){
            return new ResponseEntity<List<TransactionDetailModel>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<TransactionDetailModel>>(transactions, HttpStatus.OK);
    }
    
    //get all transaction of a user
    @RequestMapping(value="/transaction/user/{id}", method=RequestMethod.GET)
    public ResponseEntity<List<TransactionModel>> listTransactionsOfUser(@PathVariable("id") String user_id) {
        UserModel user = userServices.findById(Integer.parseInt(user_id));
        List<TransactionModel> transactions = transactionServices.findAllTransactionOfUser(user);
        if(transactions.isEmpty()){
            return new ResponseEntity<List<TransactionModel>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<TransactionModel>>(transactions, HttpStatus.OK);
    }
    
    //get all transaction detail of transaction 
    @RequestMapping(value="/transaction/detail/{id}", method=RequestMethod.GET) 
    public ResponseEntity<List<TransactionDetailModel>> listDetailsOfTransaction(@PathVariable("id") String trans_id) { 
        TransactionModel transModel = transactionServices.findById(Integer.parseInt(trans_id)); 
        List<TransactionDetailModel> details = transactionDetailServices.findAllDetailsOfTransaction(transModel); 
        if(details.isEmpty()){ 
            return new ResponseEntity<List<TransactionDetailModel>>(HttpStatus.NO_CONTENT); 
        } 
        return new ResponseEntity<List<TransactionDetailModel>>(details, HttpStatus.OK); 
    } 
    
//    get single transaction
    @RequestMapping(value="/transaction/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TransactionModel> getTransaction(@PathVariable("id") String id) {
        TransactionModel transaction = transactionServices.findById(Integer.parseInt(id));
        if(transaction==null) {
            System.out.println("Item with id" + id + " not found");
            return new ResponseEntity<TransactionModel>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<TransactionModel>(transaction, HttpStatus.OK);
    }
    
//    add transaction
    @RequestMapping(value="/transaction", method=RequestMethod.POST)
    public ResponseEntity<Void> createTransaction(@RequestBody TransactionModel transaction, UriComponentsBuilder ucBuilder) {
        System.out.println("Creating Transaction " + transaction.getTrans_id());
        transactionServices.saveTransaction(transaction);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
//    update transaction
    @RequestMapping(value = "/transaction/{id}", method = RequestMethod.PUT)
    public ResponseEntity<TransactionModel> updateTransaction(@PathVariable("id") String id, @RequestBody TransactionModel trans) {
        System.out.println("Updating item " + id);
        TransactionModel currTransaction = transactionServices.findById(Integer.parseInt(id));
        if (currTransaction==null) {
            System.out.println("Item with id" + id + " not found");
            return new ResponseEntity<TransactionModel>(HttpStatus.NOT_FOUND);
        }
        currTransaction.setStatus(trans.getStatus());
        transactionServices.updateTransaction(currTransaction);
        return new ResponseEntity<TransactionModel>(currTransaction, HttpStatus.OK);
    }
    
//    delete transaction
    @RequestMapping(value="/transaction/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<TransactionModel> removeTransaction(@PathVariable("id") String id) {
        
        TransactionModel transaction = transactionServices.findById(Integer.parseInt(id));
        if(transaction == null) {
            System.out.println("Unable to delete. Item with id " + id + " not found");
            return new ResponseEntity<TransactionModel>(HttpStatus.NOT_FOUND);
        }
        transactionServices.deleteTransactionById(Integer.parseInt(id));
        return new ResponseEntity<TransactionModel>(HttpStatus.NO_CONTENT);
    }
        
    //complete transaction - move item from cart to transaction
    //int user_id
    //String token
    @RequestMapping(value="/transaction/complete", method=RequestMethod.POST)
    public ResponseEntity<Void> completeTransaction(@RequestBody Map<String, Object> data, UriComponentsBuilder ucBuilder){
        int trans_id;
        UserModel user = userServices.findById(Integer.parseInt(data.get("user_id").toString()));
        CartModel cart = cartServices.findByUser(user);
        Date date = new Date(System.currentTimeMillis());
        List<CartDetailModel> cartDetails = cartDetailServices.findAllCartDetailOfUser(cart);
        if(cartDetails.isEmpty()) return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        TokenModel token = tokenServices.findByUser(user);
        if((System.currentTimeMillis() - token.getCreated_at().getTime()) > 10800000 && token.getToken().equals(data.get("token").toString())){
            System.out.println("Token expired");
            tokenServices.deleteTokensById(token.getToken_id());
            return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
        }
        TransactionModel transaction = new TransactionModel();
        transaction.setUser_id(userServices.findById(Integer.parseInt(data.get("user_id").toString())));
        transaction.setDate(date);
        transactionServices.saveTransaction(transaction);
        
        TransactionDetailModel transDetail = new TransactionDetailModel();
       
        for(int i = 0; i < cartDetails.size(); i++){
            transDetail.setTrans_id(transaction);
            transDetail.setItem_id(cartDetails.get(i).getItem());
            transDetail.setPrice(cartDetails.get(i).getUomconversion_id().getPrice());
            transDetail.setQty(cartDetails.get(i).getQuantity());
            transDetail.setUom_id(cartDetails.get(i).getUomconversion_id().getUom_id());
            UOMConversionModel uomConv = cartDetails.get(i).getUomconversion_id();
            uomConv.setStock(uomConv.getStock() - cartDetails.get(i).getQuantity());
            uomConvServices.updateUOMConversion(uomConv);
            transactionDetailServices.saveTransactionDetail(transDetail);
            cartDetailServices.deleteCartDetailById(cartDetails.get(i).getCartdetail_id());
        }
        
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
