/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class DefaultController {
    
    @RequestMapping(value="/", method = RequestMethod.GET)
    public String getIndexPage() {
        return "index";
    }
    
    @RequestMapping(value="/add/user", method = RequestMethod.GET)
    public String addUser() {
        return "user/add";
    }
    
    @RequestMapping(value="/update/user/{id}", method = RequestMethod.GET)
    public String updateUser(@PathVariable("id") String id, ModelMap modelMap) {
        modelMap.put("id", id);
        return "user/update";
    }
}

