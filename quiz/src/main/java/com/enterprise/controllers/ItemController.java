/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.controllers;

import com.enterprise.models.ItemModel;
import com.enterprise.models.TokenModel;
import com.enterprise.models.UOMConversionModel;
import com.enterprise.services.CategoryServices;
import com.enterprise.services.ItemServices;
import com.enterprise.services.TokenServices;
import com.enterprise.services.UOMConversionServices;
import com.enterprise.services.UOMListServices;
import com.enterprise.services.UserServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Cason Kang
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Controller
@ComponentScan("com.enterprise.services")
public class ItemController {
    
    @Autowired
    private ItemServices itemServices;
    @Autowired
    private UserServices userServices;
    @Autowired
    private CategoryServices catServices;
    @Autowired
    private UOMListServices uomServices;
    @Autowired
    private UOMConversionServices uomConvServices;
    @Autowired
    private TokenServices tokenServices;
    
    @RequestMapping(value="/shop/all", method=RequestMethod.GET)
    public ResponseEntity<List<ItemModel>> listItems(ModelMap models) {
        List<ItemModel> items = itemServices.findAllItem();
        if(items.isEmpty()){
            return new ResponseEntity<List<ItemModel>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<ItemModel>>(items, HttpStatus.OK);
    }
//    get single item
    @RequestMapping(value="/item/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ItemModel> getItem(@PathVariable("id") String id) {
        ItemModel item = itemServices.findItemById(Integer.parseInt(id));
        if(item==null) {
            System.out.println("Item with id" + id + " not found");
            return new ResponseEntity<ItemModel>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<ItemModel>(item, HttpStatus.OK);
    }
    
    //create item + uom
    /*
    data in json:
    string item_name
    int user_id
    int category_id
    int uom_id
    float price
    int stock
    string img_url
    string token
    */
    @RequestMapping(value="/item/create", method=RequestMethod.POST)
    public ResponseEntity<Void> createItemUOM(@RequestBody Map<String, Object> data, UriComponentsBuilder ucBuilder) {
        TokenModel token = tokenServices.findByUser(userServices.findById(Integer.parseInt(data.get("user_id").toString())));
        if((System.currentTimeMillis() - token.getCreated_at().getTime()) > 10800000 && token.getToken().equals(data.get("token").toString())){
            System.out.println("Token expired");
            tokenServices.deleteTokensById(token.getToken_id());
            return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
        }
        System.out.println("Creating Item " + data.get("item_name"));
        ItemModel item = new ItemModel();
        item.setItem_name(data.get("item_name").toString());
        item.setUser_id(userServices.findById(Integer.parseInt(data.get("user_id").toString())));
        item.setCategory_id(catServices.findCategoryById(Integer.parseInt(data.get("category_id").toString())));
        item.setImg_url(data.get("img_url").toString());
        itemServices.saveItem(item);
        
        UOMConversionModel uom = new UOMConversionModel();
        uom.setItem_id(item);
        uom.setUom_id(uomServices.findUOMListById(Integer.parseInt(data.get("uom_id").toString())));
        uom.setPrice(Float.parseFloat(data.get("price").toString()));
        uom.setStock(Integer.parseInt(data.get("stock").toString()));
        uomConvServices.saveUOMConversion(uom);
        
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
//    add item
    
    @RequestMapping(value="/item", method=RequestMethod.POST)
    public ResponseEntity<Void> createItem(@RequestBody Map<String, Object> data, UriComponentsBuilder ucBuilder) {
        System.out.println("Creating Item " + data.get("item_name"));
        ItemModel item = new ItemModel();
        item.setItem_name(data.get("item_name").toString());
        item.setUser_id(userServices.findById(Integer.parseInt(data.get("user_id").toString())));
        item.setCategory_id(catServices.findCategoryById(Integer.parseInt(data.get("category_id").toString())));
        item.setImg_url(data.get("img_url").toString());
        
        itemServices.saveItem(item);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
//    update
    @RequestMapping(value = "/item/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ItemModel> updateItem(@PathVariable("id") String id, @RequestBody Map<String, Object> data) {
        System.out.println("Updating item " + id);
        ItemModel currentItem = itemServices.findItemById(Integer.parseInt(id));
        if (currentItem==null) {
            System.out.println("Item with id" + id + " not found");
            return new ResponseEntity<ItemModel>(HttpStatus.NOT_FOUND);
        }
        if(currentItem.getUser_id().getUser_id() != Integer.parseInt(data.get("user_id").toString())){
            System.out.println("Not created by user");
            return new ResponseEntity<ItemModel>(HttpStatus.FORBIDDEN);
        }
        currentItem.setItem_name(data.get("item_name").toString());
        currentItem.setCategory_id(catServices.findCategoryById(Integer.parseInt(data.get("category_id").toString())));
        itemServices.updateItem(currentItem);
        return new ResponseEntity<ItemModel>(currentItem, HttpStatus.OK);
    }
    
//    delete
    @RequestMapping(value="/item/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<ItemModel> removeItem(@PathVariable("id") String id) {
        
        ItemModel item = itemServices.findItemById(Integer.parseInt(id));
        if(item == null) {
            System.out.println("Unable to delete. Item with id " + id + " not found");
            return new ResponseEntity<ItemModel>(HttpStatus.NOT_FOUND);
        }
        itemServices.deleteItemById(Integer.parseInt(id));
        return new ResponseEntity<ItemModel>(HttpStatus.NO_CONTENT);
    }
    
//    search by name
    @RequestMapping(value="/item-getbyname/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ItemModel>> getItemName(@PathVariable("name") String name) {
        List<ItemModel> item = itemServices.findItemByName(name);
        if(item.isEmpty()) {
            System.out.println("Item with id" + name + " not found");
            return new ResponseEntity<List<ItemModel>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<ItemModel>>(item, HttpStatus.OK);
    }
    
    //search by category
    @RequestMapping(value="/shop/{category}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ItemModel>> getItemCategory(@PathVariable("category") String category) {
        List<ItemModel> item = itemServices.findItemByCategory(category);
        if(item.isEmpty()) {
            System.out.println("Item with category " + category + " not found");
            return new ResponseEntity<List<ItemModel>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<ItemModel>>(item, HttpStatus.OK);
    }
    
    //insert string
    @RequestMapping(value="/item/category", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> insertCategory(@RequestBody Map<String, Object> data){
        System.out.println("Creating new item");
        itemServices.insertItemAndCategory(data.get("category_name").toString(), data.get("item_name").toString());
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
    //getUomConversions
    @RequestMapping(value="/product/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<UOMConversionModel>> getUomConversions(@PathVariable("id") int itemId){
        ItemModel item = itemServices.findItemById(itemId);
        List<UOMConversionModel> uomConversionList = uomConvServices.findByItem(item);
        if(uomConversionList.isEmpty()){
            return new ResponseEntity<List<UOMConversionModel>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<UOMConversionModel>>(uomConversionList, HttpStatus.OK);
    }
    
    //get items by user
    @RequestMapping(value="/product/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<ItemModel>> getItemsOfUser(@PathVariable("id") int userId){
        List<ItemModel> items = itemServices.findItemByUser(userServices.findById(userId));
        if(items.isEmpty()){
            return new ResponseEntity<List<ItemModel>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<ItemModel>>(items, HttpStatus.OK);
    }
}
