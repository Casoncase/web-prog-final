/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.controllers;

import com.enterprise.models.CartModel;
import com.enterprise.models.ComplaintModel;
import com.enterprise.models.ItemModel;
import com.enterprise.models.TokenModel;
import com.enterprise.models.TransactionDetailModel;
import com.enterprise.models.TransactionModel;
import com.enterprise.models.UserModel;
import com.enterprise.services.ComplaintServices;
import com.enterprise.services.ItemServices;
import com.enterprise.services.TokenServices;
import com.enterprise.services.TransactionDetailServices;
import com.enterprise.services.TransactionServices;
import com.enterprise.services.UserServices;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Cason Kang
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Controller
@ComponentScan("com.enterprise.services")
public class ComplaintController {
    
    @Autowired
    private ComplaintServices complaintServices;
    @Autowired
    private UserServices userServices;
    @Autowired
    private ItemServices itemServices;
    @Autowired
    private TransactionServices transactionServices;
    @Autowired
    private TransactionDetailServices transactionDetailServices;
    @Autowired
    private TokenServices tokenServices;
    
    //get all complaints
    @RequestMapping(value="/complaint", method=RequestMethod.GET)
    public ResponseEntity<List<ComplaintModel>> listComplaints(ModelMap models) {
        List<ComplaintModel> items = complaintServices.findAllComplaint();
        if(items.isEmpty()){
            return new ResponseEntity<List<ComplaintModel>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<ComplaintModel>>(items, HttpStatus.OK);
    }
    
    //get complaint of transaction detail
    @RequestMapping(value="/complaint/{id}", method=RequestMethod.GET)
    public ResponseEntity<ComplaintModel> getComplaintOfTransDetail(@PathVariable("id") String id, ModelMap models) {
        TransactionDetailModel transDet = transactionDetailServices.findTransactionDetailById(Integer.parseInt(id));
        ComplaintModel complaint = complaintServices.findComplaintByTransDetail(transDet);
        if(complaint == null){
            return new ResponseEntity<ComplaintModel>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<ComplaintModel>(complaint, HttpStatus.OK);
    }
    
    //get all complaints of item
    //int user_id
    //string token
    @RequestMapping(value="/complaints/{id}", method=RequestMethod.POST)
    public ResponseEntity<List<ComplaintModel>> findComplaintsOfItem(@PathVariable("id") String item_id, @RequestBody Map<String, Object> data, UriComponentsBuilder ucBuilder) {
        List<ComplaintModel> complaints = new ArrayList<ComplaintModel>();
        UserModel user = userServices.findById(Integer.parseInt(data.get("user_id").toString()));
        TokenModel token = tokenServices.findByUser(user);
        if((System.currentTimeMillis() - token.getCreated_at().getTime()) > 10800000 && token.getToken().equals(data.get("token").toString())){
            System.out.println("Token expired");
            tokenServices.deleteTokensById(token.getToken_id());
            return new ResponseEntity<List<ComplaintModel>>(HttpStatus.FORBIDDEN);
        }
        ItemModel item = itemServices.findItemById(Integer.parseInt(item_id));
        List<TransactionDetailModel> transDetails = transactionDetailServices.findTransactionDetailByItem(item);
        if(transDetails.isEmpty()) return new ResponseEntity<List<ComplaintModel>>(HttpStatus.NO_CONTENT);
        for(int i = 0; i < transDetails.size(); i++){
            complaints.add(complaintServices.findComplaintByTransDetail(transDetails.get(i)));
        }
        if(complaints.isEmpty())return new ResponseEntity<List<ComplaintModel>>(HttpStatus.NO_CONTENT);
            
        return new ResponseEntity<List<ComplaintModel>>(complaints, HttpStatus.OK);
    }
    
    //add complaint of transaction by user
    //string content
    //int user_id
    //string token
    @RequestMapping(value="/complaint/{id}", method=RequestMethod.POST)
    public ResponseEntity<Void> createComplaint(@PathVariable("id") String transactionDetail_id, @RequestBody Map<String, Object> data, UriComponentsBuilder ucBuilder) {
        ComplaintModel complaint = new ComplaintModel();
        UserModel user = userServices.findById(Integer.parseInt(data.get("user_id").toString()));
        TokenModel token = tokenServices.findByUser(user);
        if((System.currentTimeMillis() - token.getCreated_at().getTime()) > 10800000 && token.getToken().equals(data.get("token").toString())){
            System.out.println("Token expired");
            tokenServices.deleteTokensById(token.getToken_id());
            return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
        }
        TransactionDetailModel transDetail = transactionDetailServices.findTransactionDetailById(Integer.parseInt(transactionDetail_id));
        TransactionModel transaction = transDetail.getTrans_id();
        Date date = new Date(System.currentTimeMillis());
        complaint.setIssuer_id(user);
        complaint.setContent(data.get("content").toString());
        complaint.setTransactionDetail_id(transDetail);
        complaint.setDate(date);
        complaintServices.saveComplaint(complaint);
        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
//    update
    @RequestMapping(value = "/complaint/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ComplaintModel> updateComplaint(@PathVariable("id") String id, @RequestBody Map<String, Object> data) {
        System.out.println("Updating item " + id);
        ComplaintModel currCart = complaintServices.findComplaintById(Integer.parseInt(id));
        if (currCart==null) {
            System.out.println("Item with id" + id + " not found");
            return new ResponseEntity<ComplaintModel>(HttpStatus.NOT_FOUND);
        }
        complaintServices.updateComplaint(currCart);
        return new ResponseEntity<ComplaintModel>(currCart, HttpStatus.OK);
    }
    
//    delete
    @RequestMapping(value="/complaint/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<ComplaintModel> removeComplaint(@PathVariable("id") String id) {
        
        ComplaintModel complaint = complaintServices.findComplaintById(Integer.parseInt(id));
        if(complaint == null) {
            System.out.println("Unable to delete. Item with id " + id + " not found");
            return new ResponseEntity<ComplaintModel>(HttpStatus.NOT_FOUND);
        }
        complaintServices.deleteComplaintById(Integer.parseInt(id));
        return new ResponseEntity<ComplaintModel>(HttpStatus.NO_CONTENT);
    }
}
